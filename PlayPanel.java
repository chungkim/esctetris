import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JPanel;

// 
// Decompiled by Procyon v0.5.30
// 

class PlayPanel extends JPanel
{
    private static final long serialVersionUID = 1L;
    public static final Color BG_COLOR;
    private PlayManager playMgr;
    
    static {
        BG_COLOR = Color.BLACK;
    }
    
    public PlayPanel() {
        this.init();
    }
    
    public void setPlayManager(final PlayManager mgr) {
        this.playMgr = mgr;
    }
    
    public void paintComponent(final Graphics g) {
        super.paintComponent(g);
        this.playMgr.getBoard().draw(g, false);
        final Block block = this.playMgr.getCurrentBlock();
        if (block != null) {
            block.draw(g, true);
        }
    }
    
    private void init() {
        this.setPreferredSize(new Dimension(288, 480));
        this.setBackground(PlayPanel.BG_COLOR);
    }
}
