import java.util.Calendar;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.BorderLayout;
import javax.swing.KeyStroke;
import java.awt.event.KeyListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

// 
// Decompiled by Procyon v0.5.30
// 

class EscFrame extends JFrame implements ActionListener, TetrisFrame
{
    private static final long serialVersionUID = 0L;
    private static final int BORDER_SIZE = 2;
    private JMenuBar menuBar;
    private JMenu mnTetris;
    private JMenuItem miStart;
    private JCheckBoxMenuItem miPause;
    private JMenuItem miStop;
    private JMenuItem miRank;
    private JMenuItem miExit;
    private JMenu mnHelp;
    private JMenuItem miAbout;
    private PlayPanel playPanel;
    private BlockPanel blockPanel;
    private ScorePanel scorePanel;
    private PlayManager playMgr;
    private ScoreManager scoreMgr;
    
    public EscFrame() {
        this.menuBar = new JMenuBar();
        this.playPanel = new PlayPanel();
        this.blockPanel = new BlockPanel();
        this.scorePanel = new ScorePanel();
        this.playMgr = new PlayManager(this);
        try {
            this.scoreMgr = new ScoreManager(this);
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Esc Tetris", 0);
            System.exit(1);
        }
        this.playMgr.setScoreManager(this.scoreMgr);
        this.playPanel.setPlayManager(this.playMgr);
        this.blockPanel.setPlayManager(this.playMgr);
        this.scorePanel.setScoreManager(this.scoreMgr);
        this.init();
    }
    
    @Override
    public JPanel getPlayPanel() {
        return this.playPanel;
    }
    
    @Override
    public JPanel getBlockPanel() {
        return this.blockPanel;
    }
    
    @Override
    public JPanel getScorePanel() {
        return this.scorePanel;
    }
    
    @Override
    public void actionPerformed(final ActionEvent e) {
        final Object source = e.getSource();
        if (source == this.miStart) {
            this.startPerformed();
        }
        else if (source == this.miPause) {
            this.pausePerformed();
        }
        else if (source == this.miStop) {
            this.stopPerformed();
        }
        else if (source == this.miRank) {
            this.rankPerformed();
        }
        else if (source == this.miExit) {
            this.exitPerformed();
        }
        else if (source == this.miAbout) {
            this.aboutPerformed();
        }
    }
    
    private void startPerformed() {
        this.setTitle("Esc Tetris - Playing..");
        this.miStart.setEnabled(false);
        this.miStop.setEnabled(true);
        this.miPause.setEnabled(true);
        this.miPause.setSelected(false);
        this.miRank.setEnabled(false);
        this.addKeyListener(this.playMgr);
        this.playMgr.start();
    }
    
    private void pausePerformed() {
        final boolean selected = this.miPause.isSelected();
        if (selected) {
            this.removeKeyListener(this.playMgr);
            this.miRank.setEnabled(true);
            this.setTitle("Esc Tetris - Paused");
            this.playMgr.pause();
        }
        else {
            this.addKeyListener(this.playMgr);
            this.miRank.setEnabled(false);
            this.setTitle("Esc Tetris - Playing..");
            this.playMgr.resume();
        }
        this.miPause.setSelected(selected);
    }
    
    @Override
    public void stopPerformed() {
        this.setTitle("Esc Tetris - Stopped");
        this.playMgr.stop();
        this.removeKeyListener(this.playMgr);
        this.miStart.setEnabled(true);
        this.miStop.setEnabled(false);
        this.miPause.setEnabled(false);
        this.miRank.setEnabled(true);
        final Player player = this.scoreMgr.getPlayer();
        final int rank = this.scoreMgr.getPlayerRank();
        if (rank > 0) {
            String name = "";
            while (name.isEmpty()) {
                name = JOptionPane.showInputDialog(this, "Your Result\n - Level : " + player.getLevel() + "\n" + " - Lines : " + player.getLines() + "\n" + " - Time : " + this.millisToString(player.getTime()) + "\n\n" + "Congratulations!\n" + "Your Ranking is \"" + rank + "\".\n\n" + "* enter your name:", "Esc Tetris", 1);
                if (name.isEmpty()) {
                    JOptionPane.showMessageDialog(this, "Please enter your name.", "Esc Tetris", 2);
                }
            }
            player.setName(name);
            try {
                this.scoreMgr.setPlayerRank(rank);
            }
            catch (Exception e) {
                JOptionPane.showMessageDialog(this, e.getMessage(), "Esc Tetris", 0);
            }
            this.rankPerformed();
        }
        else {
            JOptionPane.showMessageDialog(this, "Your Result\n - Level : " + player.getLevel() + "\n" + " - Lines : " + player.getLines() + "\n" + " - Time : " + this.millisToString(player.getTime()) + "\n\n" + "Thank you for Playing!", "Esc Tetris", 1);
        }
    }
    
    public void rankPerformed() {
        final RankDialog dialog = new RankDialog(this.scoreMgr, this);
        dialog.setVisible(true);
    }
    
    public void aboutPerformed() {
        JOptionPane.showMessageDialog(this, "Esc Tetris\n - version 1.3\n\npowered by William Kim", "Esc Tetris", 1);
    }
    
    private void exitPerformed() {
        this.dispose();
        System.exit(0);
    }
    
    private void init() {
        this.setTitle("Esc Tetris");
        this.setLocationByPlatform(true);
        this.setResizable(false);
        this.addMenuItems();
        this.addComponents();
        this.addListeners();
        this.pack();
    }
    
    private void addMenuItems() {
        this.mnTetris = new JMenu("Tetris");
        this.mnHelp = new JMenu("Help");
        (this.miStart = new JMenuItem("Start", 83)).setAccelerator(KeyStroke.getKeyStroke(10, 0));
        this.miStart.getAccessibleContext().setAccessibleDescription("Start new Tetris.");
        (this.miPause = new JCheckBoxMenuItem("Pause", false)).setMnemonic(80);
        this.miPause.setAccelerator(KeyStroke.getKeyStroke(19, 0));
        this.miPause.getAccessibleContext().setAccessibleDescription("Pause, or resume Tetris.");
        this.miPause.setEnabled(false);
        (this.miStop = new JMenuItem("Stop", 79)).setAccelerator(KeyStroke.getKeyStroke(27, 0));
        this.miStop.getAccessibleContext().setAccessibleDescription("Stop Tetris.");
        this.miStop.setEnabled(false);
        (this.miRank = new JMenuItem("Rank", 82)).setAccelerator(KeyStroke.getKeyStroke(119, 0));
        this.miRank.getAccessibleContext().setAccessibleDescription("Shows player ranking.");
        (this.miExit = new JMenuItem("Exit", 88)).setAccelerator(KeyStroke.getKeyStroke(115, 8));
        this.miExit.getAccessibleContext().setAccessibleDescription("Exit Esc Tetris.");
        this.miAbout = new JMenuItem("About Esc Tetris", 65);
        this.miAbout.getAccessibleContext().setAccessibleDescription("Shows information about Esc Tetris.");
        this.mnTetris.add(this.miStart);
        this.mnTetris.addSeparator();
        this.mnTetris.add(this.miPause);
        this.mnTetris.add(this.miStop);
        this.mnTetris.addSeparator();
        this.mnTetris.add(this.miRank);
        this.mnTetris.addSeparator();
        this.mnTetris.add(this.miExit);
        this.mnHelp.add(this.miAbout);
        this.menuBar.add(this.mnTetris);
        this.menuBar.add(this.mnHelp);
        this.setJMenuBar(this.menuBar);
    }
    
    private void addComponents() {
        final Container con = this.getContentPane();
        final BorderLayout layout = new BorderLayout(2, 0);
        con.setLayout(layout);
        final JPanel panel = new JPanel(new BorderLayout(0, 2));
        panel.add("North", this.blockPanel);
        panel.add("Center", this.scorePanel);
        con.add("Center", this.playPanel);
        con.add("East", panel);
    }
    
    private void addListeners() {
        this.setDefaultCloseOperation(3);
        this.miStart.addActionListener(this);
        this.miStop.addActionListener(this);
        this.miPause.addActionListener(this);
        this.miRank.addActionListener(this);
        this.miExit.addActionListener(this);
        this.miAbout.addActionListener(this);
    }
    
    private String millisToString(final long millis) {
        final Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(millis);
        String s = new String();
        final int min = cal.get(12);
        if (min > 0) {
            s = String.valueOf(s) + String.valueOf(min) + " min ";
            cal.add(12, -1 * min);
        }
        final int sec = cal.get(13);
        s = String.valueOf(s) + String.valueOf(sec) + " sec";
        return s;
    }
}
