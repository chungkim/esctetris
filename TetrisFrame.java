import javax.swing.JPanel;

// 
// Decompiled by Procyon v0.5.30
// 

public interface TetrisFrame
{
    JPanel getPlayPanel();
    
    JPanel getBlockPanel();
    
    JPanel getScorePanel();
    
    void stopPerformed();
}
