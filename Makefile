JAVAC = javac
JAR = jar
JFLAGS = -g

.SUFFIXES: .java .class
.java.class:
	@$(JAVAC) $(JFLAGS) $*.java

CLASSES = \
		EscTetris.java \
		TetrisFrame.java \
		EscFrame.java \
		Player.java \
		Board.java \
		Block.java \
		Brick.java \
		PlayPanel.java \
		BlockPanel.java \
		ScoreManager.java \
		PlayManager.java \
		ScoreManager.java \
		RankDialog.java \
		RankTableModel.java

PROGRAM = EscTetris.jar
MANIFEST = Manifest.mf

default: classes $(MANIFEST)
	@$(JAR) cfm $(PROGRAM) $(MANIFEST) $(CLASSES)
	@echo $(PROGRAM)

classes: $(CLASSES:.java=.class)

clean:
	$(RM) $(PROGRAM) *.class
