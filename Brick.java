import java.awt.Color;
import java.awt.Point;
import java.awt.Graphics;

// 
// Decompiled by Procyon v0.5.30
// 

class Brick
{
    public static final int PIXEL = 24;
    public static final int BLANK = 0;
    private static final int RED = 1;
    private static final int ORANGE = 2;
    private static final int YELLOW = 3;
    private static final int GREEN = 4;
    private static final int BLUE = 5;
    private static final int INDIGO = 6;
    private static final int VIOLET = 7;
    private int value;
    
    public Brick() {
        this.value = 0;
    }
    
    public Brick(final int value) {
        this.value = value;
    }
    
    public void setValue(final int value) {
        this.value = value;
    }
    
    public int getValue() {
        return this.value;
    }
    
    public void draw(final Graphics g, final Point location, final Boolean blank, final Boolean draw, final Boolean index) {
        if (g == null || (!blank && this.value == 0)) {
            return;
        }
        Color color = PlayPanel.BG_COLOR;
        if (draw) {
            switch (this.value) {
                case 1: {
                    color = Color.RED;
                    break;
                }
                case 2: {
                    color = new Color(250, 150, 0);
                    break;
                }
                case 3: {
                    color = Color.YELLOW;
                    break;
                }
                case 4: {
                    color = Color.GREEN;
                    break;
                }
                case 5: {
                    color = Color.BLUE;
                    break;
                }
                case 6: {
                    color = Color.CYAN;
                    break;
                }
                case 7: {
                    color = new Color(150, 0, 250);
                    break;
                }
            }
        }
        final Point p = index ? indexToPixel(location) : location;
        g.setColor(color);
        g.fillRect(p.x, p.y, 24, 24);
        g.setColor(PlayPanel.BG_COLOR);
        g.drawRect(p.x, p.y, 24, 24);
    }
    
    public boolean isBlank() {
        return this.value == 0;
    }
    
    public static Point indexToPixel(final Point p) {
        return new Point(p.x * 24, p.y * 24);
    }
    
    public static Point pixelToIndex(final Point p) {
        return new Point(p.x / 24, p.y / 24);
    }
}
