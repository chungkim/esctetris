import java.awt.Graphics;
import java.util.Random;
import java.awt.Point;

// 
// Decompiled by Procyon v0.5.30
// 

class Block
{
    public static final int TYPE = 7;
    public static final int SIDE = 4;
    public static final int SIZE = 4;
    public static final Point DEFFAULT_LOCATION;
    private static Random random;
    private Brick[][][] bricks;
    private int type;
    private int side;
    private Point location;
    public static final int[][][][] blockMap;
    
    static {
        DEFFAULT_LOCATION = new Point(4, -3);
        Block.random = new Random(System.currentTimeMillis());
        final int[][][][] blockMap2 = new int[7][][][];
        blockMap2[0] = new int[][][] { { new int[4], { 1, 1, 1, 1 }, new int[4], new int[4] }, { { 0, 0, 1, 0 }, { 0, 0, 1, 0 }, { 0, 0, 1, 0 }, { 0, 0, 1, 0 } }, { new int[4], { 1, 1, 1, 1 }, new int[4], new int[4] }, { { 0, 0, 1, 0 }, { 0, 0, 1, 0 }, { 0, 0, 1, 0 }, { 0, 0, 1, 0 } } };
        final int n = 1;
        final int[][][] array = new int[4][][];
        array[0] = new int[][] { new int[4], { 1, 1, 0, 0 }, { 0, 1, 1, 0 }, new int[4] };
        final int n2 = 1;
        final int[][] array2 = { { 0, 1, 0, 0 }, { 1, 1, 0, 0 }, null, null };
        final int n3 = 2;
        final int[] array3 = new int[4];
        array3[0] = 1;
        array2[n3] = array3;
        array2[3] = new int[4];
        array[n2] = array2;
        array[2] = new int[][] { new int[4], { 1, 1, 0, 0 }, { 0, 1, 1, 0 }, new int[4] };
        final int n4 = 3;
        final int[][] array4 = { { 0, 1, 0, 0 }, { 1, 1, 0, 0 }, null, null };
        final int n5 = 2;
        final int[] array5 = new int[4];
        array5[0] = 1;
        array4[n5] = array5;
        array4[3] = new int[4];
        array[n4] = array4;
        blockMap2[n] = array;
        blockMap2[2] = new int[][][] { { new int[4], { 0, 1, 1, 0 }, { 1, 1, 0, 0 }, new int[4] }, { { 0, 1, 0, 0 }, { 0, 1, 1, 0 }, { 0, 0, 1, 0 }, new int[4] }, { new int[4], { 0, 1, 1, 0 }, { 1, 1, 0, 0 }, new int[4] }, { { 0, 1, 0, 0 }, { 0, 1, 1, 0 }, { 0, 0, 1, 0 }, new int[4] } };
        blockMap2[3] = new int[][][] { { new int[4], { 1, 1, 1, 0 }, { 0, 1, 0, 0 }, new int[4] }, { { 0, 1, 0, 0 }, { 0, 1, 1, 0 }, { 0, 1, 0, 0 }, new int[4] }, { { 0, 1, 0, 0 }, { 1, 1, 1, 0 }, new int[4], new int[4] }, { { 0, 1, 0, 0 }, { 1, 1, 0, 0 }, { 0, 1, 0, 0 }, new int[4] } };
        final int n6 = 4;
        final int[][][] array6 = new int[4][][];
        final int n7 = 0;
        final int[][] array7 = { new int[4], { 1, 1, 1, 0 }, null, null };
        final int n8 = 2;
        final int[] array8 = new int[4];
        array8[0] = 1;
        array7[n8] = array8;
        array7[3] = new int[4];
        array6[n7] = array7;
        array6[1] = new int[][] { { 0, 1, 0, 0 }, { 0, 1, 0, 0 }, { 0, 1, 1, 0 }, new int[4] };
        array6[2] = new int[][] { { 0, 0, 1, 0 }, { 1, 1, 1, 0 }, new int[4], new int[4] };
        array6[3] = new int[][] { { 1, 1, 0, 0 }, { 0, 1, 0, 0 }, { 0, 1, 0, 0 }, new int[4] };
        blockMap2[n6] = array6;
        final int n9 = 5;
        final int[][][] array9 = { { new int[4], { 1, 1, 1, 0 }, { 0, 0, 1, 0 }, new int[4] }, { { 0, 1, 1, 0 }, { 0, 1, 0, 0 }, { 0, 1, 0, 0 }, new int[4] }, null, null };
        final int n10 = 2;
        final int[][] array10 = new int[4][];
        final int n11 = 0;
        final int[] array11 = new int[4];
        array11[0] = 1;
        array10[n11] = array11;
        array10[1] = new int[] { 1, 1, 1, 0 };
        array10[2] = new int[4];
        array10[3] = new int[4];
        array9[n10] = array10;
        array9[3] = new int[][] { { 0, 1, 0, 0 }, { 0, 1, 0, 0 }, { 1, 1, 0, 0 }, new int[4] };
        blockMap2[n9] = array9;
        blockMap2[6] = new int[][][] { { new int[4], { 0, 1, 1, 0 }, { 0, 1, 1, 0 }, new int[4] }, { new int[4], { 0, 1, 1, 0 }, { 0, 1, 1, 0 }, new int[4] }, { new int[4], { 0, 1, 1, 0 }, { 0, 1, 1, 0 }, new int[4] }, { new int[4], { 0, 1, 1, 0 }, { 0, 1, 1, 0 }, new int[4] } };
        blockMap = blockMap2;
    }
    
    public Block(final int type) {
        this.type = type;
        this.bricks = new Brick[4][4][4];
        this.location = new Point(Block.DEFFAULT_LOCATION);
        this.init();
    }
    
    public Block() {
        this(getRandomType());
    }
    
    public Block(final Block block) {
        this.bricks = block.bricks.clone();
        this.type = block.type;
        this.side = block.side;
        this.location = new Point(block.location);
    }
    
    public int getType() {
        return this.type;
    }
    
    public int getSide() {
        return this.side;
    }
    
    public void setLocation(final Point location) {
        this.location = location;
    }
    
    public Point getLocation() {
        return this.location;
    }
    
    public void draw(final Graphics g, final Boolean draw) {
        for (int h = 0; h < 4; ++h) {
            for (int w = 0; w < 4; ++w) {
                final Point p = new Point(this.location.x + w, this.location.y + h);
                this.bricks[this.side][h][w].draw(g, p, false, draw, true);
            }
        }
    }
    
    public static void draw(final Graphics g, final int type, final int side, final Point pixel, final Boolean draw) {
        for (int h = 0; h < 4; ++h) {
            for (int w = 0; w < 4; ++w) {
                if (Block.blockMap[type][side][h][w] != 0) {
                    final Point p = new Point(pixel.x + w * 24, pixel.y + h * 24);
                    final Brick brick = new Brick(type + 1);
                    brick.draw(g, p, false, true, false);
                }
            }
        }
    }
    
    public void move(final Graphics g, final int key) {
        this.draw(g, false);
        switch (key) {
            case 37: {
                final Point location = this.location;
                --location.x;
                break;
            }
            case 39: {
                final Point location2 = this.location;
                ++location2.x;
                break;
            }
            case 40: {
                final Point location3 = this.location;
                ++location3.y;
                break;
            }
        }
        this.draw(g, true);
    }
    
    public void turn(final Graphics g) {
        this.draw(g, false);
        this.side = (this.side + 1) % 4;
        this.draw(g, true);
    }
    
    public boolean isBlankAt(final Point p) {
        return this.bricks[this.side][p.y][p.x].isBlank();
    }
    
    public static int getRandomType() {
        return Block.random.nextInt(7);
    }
    
    private void init() {
        for (int s = 0; s < 4; ++s) {
            for (int h = 0; h < 4; ++h) {
                for (int w = 0; w < 4; ++w) {
                    final boolean blocked = Block.blockMap[this.type][s][h][w] != 0;
                    this.bricks[s][h][w] = new Brick(blocked ? (this.type + 1) : 0);
                }
            }
        }
    }
}
