import java.awt.event.KeyEvent;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import java.awt.Point;
import java.awt.Container;
import java.awt.Component;
import javax.swing.JPanel;
import javax.swing.table.TableModel;
import java.awt.LayoutManager;
import java.awt.BorderLayout;
import java.awt.Frame;
import javax.swing.JTable;
import java.awt.Color;
import java.awt.event.KeyListener;
import javax.swing.JDialog;

// 
// Decompiled by Procyon v0.5.30
// 

public class RankDialog extends JDialog implements KeyListener
{
    private static final long serialVersionUID = 4L;
    private static final Color TABLE_BG_COLOR;
    private static final Color TABLE_FG_COLOR;
    private static final Color TABLE_GR_COLOR;
    private JTable rankTable;
    private ScoreManager scoreMgr;
    
    static {
        TABLE_BG_COLOR = new Color(16121850);
        TABLE_FG_COLOR = Color.BLACK;
        TABLE_GR_COLOR = new Color(6591981);
    }
    
    public RankDialog(final ScoreManager mgr, final EscFrame owner) {
        super(owner, "Player Ranking", true);
        this.scoreMgr = mgr;
        this.init();
    }
    
    private void init() {
        this.addComponents();
        this.addListeners();
        this.pack();
        this.setResizable(false);
        this.setLocation(this.getCenterPoint());
    }
    
    private void addComponents() {
        final Container con = this.getContentPane();
        final BorderLayout layout = new BorderLayout(0, 0);
        con.setLayout(layout);
        final Player[] rank = this.scoreMgr.getRank();
        (this.rankTable = new JTable(new RankTableModel(rank))).setBackground(RankDialog.TABLE_BG_COLOR);
        this.rankTable.setForeground(RankDialog.TABLE_FG_COLOR);
        this.rankTable.setGridColor(RankDialog.TABLE_GR_COLOR);
        this.setColumnSizes(this.rankTable);
        final JPanel panel = new JPanel(new BorderLayout(0, 0));
        panel.add("Center", this.rankTable);
        con.add("Center", panel);
    }
    
    private void addListeners() {
        this.setDefaultCloseOperation(2);
        this.rankTable.addKeyListener(this);
    }
    
    private Point getCenterPoint() {
        final int x = this.getOwner().getX() + (this.getOwner().getWidth() - this.getWidth()) / 2;
        final int y = this.getOwner().getY() + (this.getOwner().getHeight() - this.getHeight()) / 2;
        return new Point(x, y);
    }
    
    private void setColumnSizes(final JTable table) {
        final RankTableModel model = (RankTableModel)table.getModel();
        TableColumn column = null;
        Component comp = null;
        int headerWidth = 0;
        int cellWidth = 0;
        final Object[] longValues = model.longValues;
        final TableCellRenderer headerRenderer = table.getTableHeader().getDefaultRenderer();
        for (int i = 0; i < 5; ++i) {
            column = table.getColumnModel().getColumn(i);
            comp = headerRenderer.getTableCellRendererComponent(null, column.getHeaderValue(), false, false, 0, 0);
            headerWidth = comp.getPreferredSize().width;
            comp = table.getDefaultRenderer(model.getColumnClass(i)).getTableCellRendererComponent(table, longValues[i], false, false, 0, i);
            cellWidth = comp.getPreferredSize().width;
            column.setPreferredWidth(Math.max(headerWidth, cellWidth));
        }
    }
    
    @Override
    public void keyPressed(final KeyEvent e) {
    }
    
    @Override
    public void keyReleased(final KeyEvent e) {
        final int key = e.getKeyCode();
        if (key == 27) {
            this.dispose();
        }
    }
    
    @Override
    public void keyTyped(final KeyEvent e) {
    }
}
