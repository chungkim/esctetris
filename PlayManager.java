import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.Timer;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;

// 
// Decompiled by Procyon v0.5.30
// 

class PlayManager extends KeyAdapter implements ActionListener
{
    public static final int DEFAULT_BLOCK_DELAY = 500;
    public static final int MINIMUM_BLOCK_DELAY = 50;
    private TetrisFrame frame;
    private ScoreManager scoreMgr;
    private Board board;
    private Block curBlock;
    private Block nextBlock;
    private Timer timer;
    
    public PlayManager(final TetrisFrame frame) {
        this.frame = frame;
        this.board = new Board();
        this.timer = new Timer(0, this);
        this.init();
    }
    
    public void finalize() {
        this.timer.stop();
    }
    
    public void setScoreManager(final ScoreManager mgr) {
        this.scoreMgr = mgr;
    }
    
    public Board getBoard() {
        return this.board;
    }
    
    public Block getCurrentBlock() {
        return this.curBlock;
    }
    
    public Block getNextBlock() {
        return this.nextBlock;
    }
    
    public void start() {
        if (this.scoreMgr != null) {
            this.scoreMgr.start();
        }
        this.board.clear();
        this.curBlock = new Block();
        this.nextBlock = new Block();
        this.board.draw(this.frame.getPlayPanel().getGraphics(), true);
        this.frame.getBlockPanel().repaint();
        this.timer.setDelay(500);
        this.timer.start();
        this.frame.getScorePanel().repaint();
    }
    
    public void stop() {
        this.timer.stop();
        this.curBlock = null;
        this.board.draw(this.frame.getPlayPanel().getGraphics(), true);
        if (this.scoreMgr != null) {
            this.scoreMgr.stop();
        }
    }
    
    public void pause() {
        this.timer.stop();
        this.scoreMgr.pause();
    }
    
    public void resume() {
        this.scoreMgr.resume();
        this.timer.restart();
    }
    
    @Override
    public void keyPressed(final KeyEvent e) {
        final int key = e.getKeyCode();
        if (this.isKeyValid(key)) {
            this.play(key);
        }
    }
    
    @Override
    public void actionPerformed(final ActionEvent e) {
        this.play(40);
    }
    
    private void play(final int key) {
        boolean locate = true;
        if (key == 32) {
            while (this.doBlock(40)) {}
        }
        else {
            locate = !this.doBlock(key);
        }
        if (locate) {
            this.board.locate(this.curBlock);
            final int n = this.board.arrange();
            if (this.scoreMgr != null) {
                this.levelUp(n);
            }
            this.board.draw(this.frame.getPlayPanel().getGraphics(), true);
            if (this.board.isFull()) {
                this.frame.stopPerformed();
            }
            else {
                this.curBlock = this.nextBlock;
                this.nextBlock = new Block();
                this.frame.getBlockPanel().repaint();
            }
        }
    }
    
    private boolean doBlock(final int key) {
        final Block pre = new Block(this.curBlock);
        if (key == 38) {
            pre.turn(null);
            if (this.board.canLocate(pre)) {
                this.curBlock.turn(this.frame.getPlayPanel().getGraphics());
            }
        }
        else {
            pre.move(null, key);
            if (this.board.canLocate(pre)) {
                this.curBlock.move(this.frame.getPlayPanel().getGraphics(), key);
            }
            else if (key == 40) {
                return false;
            }
        }
        return true;
    }
    
    private void levelUp(final long clear) {
        int delay = this.timer.getDelay();
        delay -= (int)this.scoreMgr.levelUp(clear, delay);
        this.timer.setDelay(delay);
    }
    
    private boolean isKeyValid(final int key) {
        return key == 37 || key == 39 || key == 38 || key == 40 || key == 32;
    }
    
    private void init() {
        this.timer.setInitialDelay(0);
    }
}
