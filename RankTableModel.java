import java.util.Calendar;
import javax.swing.table.AbstractTableModel;

// 
// Decompiled by Procyon v0.5.30
// 

class RankTableModel extends AbstractTableModel
{
    private static final long serialVersionUID = 5L;
    public final Object[] longValues;
    private String[] columnNames;
    private Object[][] data;
    
    public RankTableModel(final Player[] rank) {
        this.longValues = new Object[] { 5, "XXXXXXXXXX", "Level 00", "00 Lines ", "00:00 " };
        this.columnNames = new String[] { "Rank", "Name", "Level", "Lines", "Time" };
        this.data = new Object[rank.length][this.columnNames.length];
        for (int i = 0; i < rank.length; ++i) {
            this.data[i][0] = i + 1;
            this.data[i][1] = rank[i].getName();
            this.data[i][2] = "Level " + rank[i].getLevel();
            this.data[i][3] = String.valueOf(rank[i].getLines()) + " Lines";
            this.data[i][4] = this.millisToString(rank[i].getTime());
        }
    }
    
    @Override
    public int getColumnCount() {
        return this.columnNames.length;
    }
    
    @Override
    public int getRowCount() {
        return this.data.length;
    }
    
    @Override
    public String getColumnName(final int col) {
        return this.columnNames[col];
    }
    
    @Override
    public Object getValueAt(final int row, final int col) {
        return this.data[row][col];
    }
    
    @Override
    public boolean isCellEditable(final int row, final int col) {
        return false;
    }
    
    private String millisToString(final long millis) {
        final Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(millis);
        String s = new String();
        final int min = cal.get(12);
        s = String.valueOf(s) + String.valueOf(min / 10);
        s = String.valueOf(s) + String.valueOf(min % 10);
        s = String.valueOf(s) + ":";
        cal.add(12, -1 * min);
        final int sec = cal.get(13);
        s = String.valueOf(s) + String.valueOf(sec / 10);
        s = String.valueOf(s) + String.valueOf(sec % 10);
        return s;
    }
}
