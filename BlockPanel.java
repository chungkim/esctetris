import java.awt.Dimension;
import java.awt.Point;
import java.awt.Graphics;
import javax.swing.JPanel;

// 
// Decompiled by Procyon v0.5.30
// 

public class BlockPanel extends JPanel
{
    private static final long serialVersionUID = 2L;
    private PlayManager playMgr;
    
    public BlockPanel() {
        this.init();
    }
    
    public void setPlayManager(final PlayManager mgr) {
        this.playMgr = mgr;
    }
    
    public void paintComponent(final Graphics g) {
        super.paintComponent(g);
        final Block block = this.playMgr.getNextBlock();
        if (block != null) {
            final int type = block.getType();
            Point p;
            if (type == 0) {
                p = new Point(12, 12);
            }
            else if (type == 6) {
                p = new Point(12, 0);
            }
            else {
                p = new Point(24, 0);
            }
            Block.draw(g, block.getType(), block.getSide(), p, true);
        }
    }
    
    private void init() {
        this.setPreferredSize(new Dimension(120, 96));
        this.setBackground(PlayPanel.BG_COLOR);
    }
}
