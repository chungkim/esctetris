import java.io.OutputStream;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.File;
import javax.swing.Timer;
import java.awt.event.ActionListener;

// 
// Decompiled by Procyon v0.5.30
// 

public class ScoreManager implements ActionListener
{
    public static final int LEVEL_UP_DELAY = 1000;
    public static final int LEVEL_UP_AMOUNT = 30;
    public static final int LEVEL_UP_LINES = 3;
    public static final int RANK_COUNT = 5;
    public static final String RANK_FILE = "EscTetris.rank";
    private TetrisFrame frame;
    private Timer timer;
    private Player player;
    private Player[] rank;
    private File rankFile;
    
    public ScoreManager(final TetrisFrame frame) throws IOException, ClassNotFoundException {
        this.frame = frame;
        this.timer = new Timer(0, this);
        this.rank = new Player[5];
        this.rankFile = new File("EscTetris.rank");
        this.init();
    }
    
    public void finalize() {
        this.timer.stop();
    }
    
    public Player getPlayer() {
        return this.player;
    }
    
    public Player[] getRank() {
        return this.rank;
    }
    
    @Override
    public void actionPerformed(final ActionEvent e) {
        this.player.incTime(1000L);
        this.frame.getScorePanel().repaint();
    }
    
    void start() {
        this.player = new Player();
        this.timer.setDelay(1000);
        this.timer.start();
    }
    
    void stop() {
        this.timer.stop();
    }
    
    void pause() {
        this.timer.stop();
    }
    
    void resume() {
        this.timer.restart();
    }
    
    long levelUp(final long clear, final long delay) {
        long amount = 0L;
        if (clear > 0L) {
            this.player.incLines(clear);
            this.frame.getScorePanel().repaint();
            if (this.canLevelUp() && delay > 50L) {
                if (delay - 50L < 30L) {
                    amount = delay - 50L;
                }
                else {
                    amount = 30L;
                }
                this.player.incLevel();
            }
        }
        return amount;
    }
    
    public int getPlayerRank() {
        int num = 0;
        for (int i = 0; i < this.rank.length; ++i) {
            if (this.rank[i].getLevel() < this.player.getLevel()) {
                num = i + 1;
                break;
            }
            if (this.rank[i].getLevel() == this.player.getLevel()) {
                if (this.rank[i].getLines() < this.player.getLines()) {
                    num = i + 1;
                    break;
                }
                if (this.rank[i].getLines() == this.player.getLines() && this.rank[i].getTime() > this.player.getTime()) {
                    num = i + 1;
                    break;
                }
            }
        }
        return num;
    }
    
    public void setPlayerRank(int num) throws FileNotFoundException, IOException {
        --num;
        for (int i = this.rank.length - 1; i > num; --i) {
            this.rank[i] = this.rank[i - 1];
        }
        this.rank[num] = this.player;
        this.writeRank();
    }
    
    private boolean canLevelUp() {
        return this.player != null && this.player.getLines() / 3L >= this.player.getLevel();
    }
    
    private void readRank() throws IOException, ClassNotFoundException {
        final ObjectInputStream in = new ObjectInputStream(new FileInputStream(this.rankFile));
        try {
            for (int i = 0; i < this.rank.length; ++i) {
                this.rank[i] = (Player)in.readObject();
            }
        }
        finally {
            in.close();
        }
        in.close();
    }
    
    private void writeRank() throws FileNotFoundException, IOException {
        final ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(this.rankFile));
        try {
            for (int i = 0; i < this.rank.length; ++i) {
                out.writeObject(this.rank[i]);
            }
        }
        finally {
            out.flush();
            out.close();
        }
        out.flush();
        out.close();
    }
    
    private void init() throws IOException, ClassNotFoundException {
        this.timer.setInitialDelay(0);
        if (this.rankFile.exists()) {
            this.readRank();
        }
        else {
            for (int i = 0; i < this.rank.length; ++i) {
                this.rank[i] = new Player();
            }
            this.rankFile.createNewFile();
            this.writeRank();
        }
    }
}
