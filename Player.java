import java.io.Serializable;

// 
// Decompiled by Procyon v0.5.30
// 

public class Player implements Serializable
{
    private static final long serialVersionUID = 6L;
    private static final String DEFAULT_NAME = "No Name";
    private String name;
    private long level;
    private long lines;
    private long time;
    
    public Player() {
        this.name = "No Name";
        this.level = 1L;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public long getLevel() {
        return this.level;
    }
    
    public long getLines() {
        return this.lines;
    }
    
    public long getTime() {
        return this.time;
    }
    
    public void incLevel() {
        ++this.level;
    }
    
    public void incLines(final long count) {
        this.lines += count;
    }
    
    public void incTime(final long amount) {
        this.time += amount;
    }
}
