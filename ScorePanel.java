import java.util.Calendar;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Point;
import javax.swing.JPanel;

// 
// Decompiled by Procyon v0.5.30
// 

public class ScorePanel extends JPanel
{
    private static final long serialVersionUID = 3L;
    public static final Point LEVEL_POSITION;
    public static final Point LINES_POSITION;
    public static final Point TIME_POSITION;
    public static final Color BG_COLOR;
    private ScoreManager scoreMgr;
    
    static {
        LEVEL_POSITION = new Point(8, 20);
        LINES_POSITION = new Point(8, 40);
        TIME_POSITION = new Point(8, 60);
        BG_COLOR = Color.BLACK;
    }
    
    public ScorePanel() {
        this.init();
    }
    
    public void setScoreManager(final ScoreManager mgr) {
        this.scoreMgr = mgr;
    }
    
    public void paintComponent(final Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.WHITE);
        final Player player = this.scoreMgr.getPlayer();
        if (player != null) {
            g.drawString("Level " + player.getLevel(), ScorePanel.LEVEL_POSITION.x, ScorePanel.LEVEL_POSITION.y);
            g.drawString(String.valueOf(player.getLines()) + " Lines", ScorePanel.LINES_POSITION.x, ScorePanel.LINES_POSITION.y);
            g.drawString(this.millisToString(player.getTime()), ScorePanel.TIME_POSITION.x, ScorePanel.TIME_POSITION.y);
        }
    }
    
    private void init() {
        this.setBackground(ScorePanel.BG_COLOR);
    }
    
    private String millisToString(final long millis) {
        final Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(millis);
        String s = new String();
        final int min = cal.get(12);
        s = String.valueOf(s) + String.valueOf(min / 10);
        s = String.valueOf(s) + String.valueOf(min % 10);
        s = String.valueOf(s) + ":";
        cal.add(12, -1 * min);
        final int sec = cal.get(13);
        s = String.valueOf(s) + String.valueOf(sec / 10);
        s = String.valueOf(s) + String.valueOf(sec % 10);
        return s;
    }
}
