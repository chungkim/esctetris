import java.awt.Graphics;
import java.awt.Point;

// 
// Decompiled by Procyon v0.5.30
// 

class Board
{
    public static final int WIDTH = 12;
    public static final int HEIGHT = 20;
    private Brick[][] bricks;
    
    public Board() {
        this.bricks = new Brick[20][12];
        for (int h = 0; h < 20; ++h) {
            for (int w = 0; w < 12; ++w) {
                this.bricks[h][w] = new Brick();
            }
        }
    }
    
    public boolean locate(final Block block) {
        boolean overflowed = false;
        final Point location = block.getLocation();
        for (int h = 0; h < 4; ++h) {
            for (int w = 0; w < 4; ++w) {
                if (!block.isBlankAt(new Point(w, h))) {
                    final Point p = new Point(location.x + w, location.y + h);
                    if (p.y < 0) {
                        overflowed = true;
                    }
                    else if (this.bricks[p.y][p.x].isBlank()) {
                        this.bricks[p.y][p.x].setValue(block.getType() + 1);
                    }
                }
            }
        }
        return !overflowed;
    }
    
    public int arrange() {
        int lines = 0;
        for (int l = 19; l >= 0; --l) {
            while (this.isLineCompleted(l)) {
                for (int h = l; h >= 1; --h) {
                    for (int w = 0; w < 12; ++w) {
                        this.bricks[h][w].setValue(this.bricks[h - 1][w].getValue());
                    }
                }
                ++lines;
            }
        }
        return lines;
    }
    
    public void clear() {
        for (int h = 0; h < 20; ++h) {
            for (int w = 0; w < 12; ++w) {
                this.bricks[h][w].setValue(0);
            }
        }
    }
    
    public void draw(final Graphics g, final Boolean blank) {
        for (int h = 0; h < 20; ++h) {
            for (int w = 0; w < 12; ++w) {
                this.bricks[h][w].draw(g, new Point(w, h), blank, true, true);
            }
        }
    }
    
    public boolean canLocate(final Block block) {
        final Point location = block.getLocation();
        for (int h = 0; h < 4; ++h) {
            for (int w = 0; w < 4; ++w) {
                if (!block.isBlankAt(new Point(w, h))) {
                    final Point p = new Point(location.x + w, location.y + h);
                    if (!isEntry(p) && (!isRange(p) || !this.bricks[p.y][p.x].isBlank())) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    
    public boolean isFull() {
        for (int w = 0; w < 12; ++w) {
            if (!this.bricks[0][w].isBlank()) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean isRange(final Point p) {
        return p.x >= 0 && p.x < 12 && p.y >= 0 && p.y < 20;
    }
    
    public static boolean isEntry(final Point p) {
        return p.x >= 0 && p.x < 12 && p.y >= Block.DEFFAULT_LOCATION.y && p.y < 0;
    }
    
    private boolean isLineCompleted(final int line) {
        for (int w = 0; w < 12; ++w) {
            if (this.bricks[line][w].isBlank()) {
                return false;
            }
        }
        return true;
    }
}
